resource "kubernetes_daemonset" "ravi1" {
  metadata {
    name      = "terraform-example"
    namespace = "something"
    labels = {
      test = "MyExampleApp"
    }
  }
  
  spec {
    selector {
      match_labels = {
        test = "MyExampleApp"
      }
    }
  }
}
